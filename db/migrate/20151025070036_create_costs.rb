class CreateCosts < ActiveRecord::Migration
  def change
    create_table :costs do |t|
      t.integer :amount
      t.integer :category_id
      t.string :name

      t.timestamps
    end
  end
end
