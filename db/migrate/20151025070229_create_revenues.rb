class CreateRevenues < ActiveRecord::Migration
  def change
    create_table :revenues do |t|
      t.integer :category_id
      t.string :name
      t.integer :amount

      t.timestamps
    end
  end
end
