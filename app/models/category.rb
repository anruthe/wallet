class Category < ActiveRecord::Base
has_many :revenues, dependent: :destroy
has_many :costs, dependent: :destroy
self.inheritance_column = nil

end
