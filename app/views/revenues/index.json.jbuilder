json.array!(@revenues) do |revenue|
  json.extract! revenue, :id, :category_id, :name, :amount
  json.url revenue_url(revenue, format: :json)
end
