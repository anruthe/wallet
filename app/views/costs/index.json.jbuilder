json.array!(@costs) do |cost|
  json.extract! cost, :id, :amount, :category_id, :name
  json.url cost_url(cost, format: :json)
end
